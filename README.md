Quick and dirty attempt to create a working development environment to compile dvb drivers for
a Synology NAS and DSM 6.2. It only supports x86_64 (apollolake), but you can easily modify it for other architectures.

1. Clone repository and build docker image (you need git-lfs). Don't use the image available on Dockerhub. 
```bash
git clone --recurse-submodules https://github.com/tcamargo/dsm-media_build.git
cd dsm-media_build && docker build -t discworld/dsm-media_build .
```

2. Run container
```bash
docker run --rm -it -v $(pwd)/../dsm-media_build:/dsm-media_build discworld/dsm-media_build
```

3. Compile media drivers
```bash
cd /dsm-media_build/media_build
export CROSS_COMPILE=/usr/local/x86_64-pc-linux-gnu/bin/x86_64-pc-linux-gnu-
make release DIR=/usr/local/x86_64-pc-linux-gnu/x86_64-pc-linux-gnu/sys-root/usr/lib/modules/DSM-6.2/build
./build
```

CEC_GPIO is not supported in kernel 4.4. I submitted a patch to media_build, but it was ignored. Patch media_build/v4l/versions.txt if compilation failed and execute step 3 again.
```bash
cd /dsm-media_build/media_build
patch -p1 < ../version-txt.patch
```

A recent driver package started to complain about functions fwnode_property_count_u32/64 being undefined. If you got these errors, add the following line to /dsm-media_build/media_build/v4l/config-mycompat.h and redo step 3.
```text
#define CONFIG_OF 1
```

4. Install
```bash
make install
```
Modules will go to /lib/modules/<kernel_version>. Just copy this directory to your Synology NAS. To avoid messing with standard modules, I copied all files to my home directory and used a script (/usr/local/etc/rc.d/modules.sh) to load the necessary modules during startup. Mind the loading order (check modules.dep) and DVB frontend drivers (see dmesg).

```bash
MODULES_DIR="/var/services/homes/camargo/media_build"
MODULES_START="kernel/drivers/media/mc/mc.ko kernel/drivers/media/dvb-frontends/tda10048.ko kernel/drivers/media/tuners/tda827x.ko kernel/drivers/media/dvb-frontends/tda10023.ko kernel/drivers/media/rc/rc-core.ko kernel/drivers/media/media.ko kernel/drivers/media/v4l2-core/videodev.ko kernel/drivers/media/common/videobuf2/videobuf2-common.ko kernel/drivers/media/common/videobuf2/videobuf2-memops.ko kernel/drivers/media/common/videobuf2/videobuf2-vmalloc.ko kernel/drivers/media/dvb-core/dvb-core.ko kernel/drivers/media/usb/dvb-usb/dvb-usb.ko kernel/drivers/media/usb/dvb-usb/dvb-usb-ttusb2.ko kernel/drivers/media/dvb-frontends/si2168.ko ./kernel/drivers/media/tuners/si2157.ko kernel/drivers/media/v4l2-core/v4l2-common.ko  kernel/drivers/media/common/tveeprom.ko kernel/drivers/media/usb/em28xx/em28xx.ko kernel/drivers/media/usb/em28xx/em28xx-dvb.ko"
MODULES_STOP=""

start_modules() {
	echo "--- Load modules ---"
	for i in $MODULES_START; do
		echo "Loading $i"
		insmod $MODULES_DIR/$i
	done }

stop_modules() {
	echo "--- Unload modules ---"
	for i in $MODULES_STOP; do
		echo "Unloading $i"
		rmmod $MODULES_DIR/$i
	done
}
case "$1" in
	start) start_modules ;;
	stop) stop_modules ;;
	*) echo "usage: $0 { start | stop }" >&2 exit 1 ;;
esac
```
If you come here looking for ways to create 3rd-party packages for Synology NAS products, I ask you to check the official documentation:
https://originhelp.synology.com/developer-guide/index.html
